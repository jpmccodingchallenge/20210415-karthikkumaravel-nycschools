# NYC High Schools

Native iOS app that displays a list of high schools in NYC. Upon selection of individual item from the list, a new screen is pushed revealing more details of that particular school.

Compatible Versions: iOS 13.0 and above

Combatible Devices: iPhone and iPad

Codebase: Swift

IDE: Xcode 12.4
