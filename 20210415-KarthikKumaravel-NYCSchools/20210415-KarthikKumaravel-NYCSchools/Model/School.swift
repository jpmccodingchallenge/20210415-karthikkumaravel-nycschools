//
//  School.swift
//  20210415-KarthikKumaravel-NYCSchools
//
//  Created by Karthik Kumaravel on 4/15/21.
//

import Foundation

struct School: Codable {
    var code: String?
    var schoolName: String?
    var overview: String?
    var city: String?
    var address: String?
    var zip: String?
    var state: String?
    var requirement1_1: String?
    var requirement2_1: String?
    var email: String?
    var phone_number: String?
    var website: String?
    var dict: [String: String]?
    
    init(dictionary: [String: Any]) {
        guard let dataSource = dictionary as? [String: String] else {
            return
        }
        self.code = dataSource["dbn"]
        self.schoolName = dataSource["school_name"]
        self.overview = dataSource["overview_paragraph"]
        self.city = dataSource["city"]
        self.address = dataSource["primary_address_line_1"]
        self.zip = dataSource["zip"]
        self.state = dataSource["state_code"]
        self.requirement1_1 = dataSource["requirement1_1"]
        self.requirement2_1 = dataSource["requirement2_1"]
        self.email = dataSource["school_email"]
        self.phone_number = dataSource["phone_number"]
        self.website = dataSource["website"]
        self.dict = dataSource
      }
}

//Util method to convert School object to dictionary
struct JSON {
    static let encoder = JSONEncoder()
}
extension Encodable {
    subscript(key: String) -> Any? {
        return dictionary[key]
    }
    var dictionary: [String: Any] {
        return (try? JSONSerialization.jsonObject(with: JSON.encoder.encode(self))) as? [String: Any] ?? [:]
    }
}
