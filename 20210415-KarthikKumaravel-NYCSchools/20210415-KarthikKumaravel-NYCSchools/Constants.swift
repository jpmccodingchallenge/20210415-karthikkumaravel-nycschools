//
//  Constants.swift
//  20210415-KarthikKumaravel-NYCSchools
//
//  Created by Karthik Kumaravel on 4/15/21.
//

import Foundation

struct Constants {
    static let nycHighSchools = "NYC High Schools"
    static let configuration = "Configuration"
    static let plist = "plist"
    static let appToken = "AppToken"
    static let tokenHeader = "X-App-Token"
    static let schoolList = "SchoolList"
    static let schoolDetails = "SchoolDetails"

}
