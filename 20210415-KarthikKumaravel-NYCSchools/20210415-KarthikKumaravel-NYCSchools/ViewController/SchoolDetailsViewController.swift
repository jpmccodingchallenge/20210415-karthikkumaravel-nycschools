//
//  SchoolDetailsViewController.swift
//  20210415-KarthikKumaravel-NYCSchools
//
//  Created by Karthik Kumaravel on 4/16/21.
//

import UIKit

class SchoolDetailsViewController: UIViewController {
    var school: School?
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var loadMorebutton: UIButton!
    
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var loadingView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "School Details"
        
        titleLabel.text = school?.schoolName
        descriptionLabel.text = school?.city
        loadDescription()
    }
    
    private func getDescriptionTextForTextView() -> String {
        guard let dataDict = school?.dict else {
            return ""
        }
        textView.text = ""
        var content = ""
        for item in dataDict {
            content = content + "\n" + item.key.uppercased() + ":\n" + item.value + "\n\n"
        }
        
        return content
    }
    
    private func loadDescription() {
        self.textView.text = getDescriptionTextForTextView()
    }
    
    @IBAction func loadMoreClicked(_ sender: Any) {
        guard let dbn = school?.code else {
            return
        }
        loadingView.isHidden = false
        ServiceManager.sharedInstance.getSchoolDetails(dbn) {[weak self] (schoolDetails) in
            self?.school = schoolDetails
            self?.loadDescription()
            self?.loadMorebutton.isHidden = true
        } failure: {[weak self] (error) in
            AppUtil.shared.showAlert("Error", message: error.localizedDescription, presentingController: self)
        }
    }
}
