//
//  ViewController.swift
//  20210415-KarthikKumaravel-NYCSchools
//
//  Created by Karthik Kumaravel on 4/15/21.
//

import UIKit

class SchoolListViewController: UIViewController {
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet var tapGesture: UITapGestureRecognizer!
    
    var schoolList: Array<School>?
    var selectedSchool: School?
    var searchArray: Array<School>?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = Constants.nycHighSchools
        ServiceManager.sharedInstance.getSchoolList {[weak self] (response) in
            self?.schoolList = response
            self?.searchArray = response
            self?.tableView.reloadData()
            self?.loadingView.isHidden = true
        } failure: {[weak self] (error) in
            AppUtil.shared.showAlert("Error", message: error.localizedDescription, presentingController: self)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let detailsVC = segue.destination as? SchoolDetailsViewController {
            detailsVC.school = selectedSchool
        }
    }
    
    var dataSource: Array<School>? {
        return isfiltering ? searchArray : schoolList
    }
    
    @IBAction func tapAction(_ sender: Any) {
        searchBar.resignFirstResponder()
    }

}

extension SchoolListViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let schoolItem = dataSource?[indexPath.row] else {
            return UITableViewCell()
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "SchoolListCell", for: indexPath)
        
        cell.textLabel?.text = schoolItem.schoolName
        cell.detailTextLabel?.text = schoolItem.city
        cell.imageView?.image = nil
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let schoolItem = dataSource?[indexPath.row] else {
            return
        }
        //push to details screen
        selectedSchool = schoolItem
        self.performSegue(withIdentifier: "pushToDetails", sender: nil)
    }
}

extension SchoolListViewController: UISearchBarDelegate {
    
    var isfiltering: Bool {
        return searchBar.isFocused || (nil != searchBar.text && !searchBar.text!.isEmpty)
    }
    
    fileprivate func filterSearchResult(_ searchText: String?) {
        guard let text = searchText, !text.isEmpty else {
            resetTable()
            return
        }

        searchArray = schoolList?.filter { ($0.schoolName?.contains(text) ?? false) }
        tableView.reloadData()
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        tapGesture.isEnabled = true
    }

    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        tapGesture.isEnabled = false

        guard let text = searchBar.text else {
            resetTable()
            return
        }
        
        filterSearchResult(text)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        filterSearchResult(searchBar.text)
        searchBar.resignFirstResponder()
    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
        searchBar.resignFirstResponder()
        resetTable()
    }

    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let searchStr = NSString(string: searchBar.text!).replacingCharacters(in: range, with: text)
        filterSearchResult(String(searchStr))
        return true
    }

    private func resetTable() {
        searchArray = schoolList
        tableView.reloadData()
    }

}

