//
//  AppUtil.swift
//  20210415-KarthikKumaravel-NYCSchools
//
//  Created by Karthik Kumaravel on 4/16/21.
//

import UIKit

class AppUtil {
    static let shared = AppUtil()
    
    func showAlert(_ title: String, message: String, presentingController: UIViewController?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        presentingController?.present(alert, animated: true, completion: nil)
    }
}
