//
//  ServiceManager.swift
//  20210415-KarthikKumaravel-NYCSchools
//
//  Created by Karthik Kumaravel on 4/15/21.
//

import Foundation

class ServiceManager: NSObject {
    var transactions : NSMutableArray?
    private var urls: NSDictionary?
    private let httpClient: RestClient
    private var appToken: String?
    static let sharedInstance = ServiceManager()

    override init() {
        httpClient = RestClient()
        if let path = Bundle.main.path(forResource: Constants.configuration, ofType: Constants.plist) {
            urls = NSDictionary(contentsOfFile: path)
            appToken = urls?[Constants.appToken] as? String
        }
        super.init()
    }
    
    public func getSchoolList(_ completion: @escaping (_ result: Array<School>?) -> Void, failure: @escaping (_ error: NSError) -> Void) {
        guard let urlString = self.urls?[Constants.schoolList] as? String else {
            return
        }
        
        makeServiceCall(urlString: urlString, completion: { (response: Array<School>?) in
            completion(response)
        }, failure: failure)
    }
    
    public func getSchoolDetails(_ dbn: String, completion: @escaping (_ result: School?) -> Void, failure: @escaping (_ error: NSError) -> Void) {
        guard var urlString = self.urls?[Constants.schoolDetails] as? String else {
            return
        }
        urlString = urlString + dbn
        makeServiceCall(urlString: urlString, completion: { (response: Array<School>?) in
            completion(response?.first)
        }, failure: failure)
    }
    
    public func makeServiceCall(urlString:String, completion: @escaping (_ result: Array<School>?) -> Void, failure: @escaping (_ error: NSError) -> Void) {
        if (!Reachability.isNetworkReachable()) {//Check for internet connection
            failure(NSError(domain: "No internet connection", code: 121, userInfo: nil))
            return
        }
        DispatchQueue.global(qos: DispatchQoS.QoSClass.default).async(execute: {[weak self] () -> Void  in
            var request = URLRequest(url: URL(string: urlString)!)
            request.httpMethod = "GET"
            request.addValue(self?.appToken ?? "", forHTTPHeaderField: Constants.tokenHeader)
            self?.httpClient.makeGetRequest(request: request, completion: { (jsonResponse : NSData) in
                do {
                    //convert data to AnyObject
                    let jsonObject: AnyObject? = try JSONSerialization.jsonObject(with: jsonResponse as Data, options: []) as AnyObject
                    let response = self?.parseJsonForGetSchoolList(response:jsonObject!)
                    DispatchQueue.main.async {
                        completion(response)
                    }
                }catch {
                    DispatchQueue.main.sync(execute: { () -> Void in
                        failure(NSError(domain: "Parsing Error", code: 121, userInfo: nil))
                    })
                }
            }, failure: { (error : NSError) in
                DispatchQueue.main.sync(execute: { () -> Void in
                    failure(error)//Throw general failures
                })
            })
        })
    }
    
    //This method will parse the json and save in School Model
    //Only required fields are saved into the object
    private func parseJsonForGetSchoolList(response:AnyObject) -> Array<School>? {
        
        var schoolList:Array<School> = []
        
        guard let schools = response as? NSArray else {
            return schoolList
        }
        for item in schools {
            if let schoolDict = item as? [String: Any] {
                schoolList.append(School(dictionary: schoolDict))
            }
        }
        return schoolList
    }
}
