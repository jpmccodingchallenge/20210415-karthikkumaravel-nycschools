//
//  _0210415_KarthikKumaravel_NYCSchoolsTests.swift
//  20210415-KarthikKumaravel-NYCSchoolsTests
//
//  Created by Karthik Kumaravel on 4/15/21.
//

import XCTest
@testable import _0210415_KarthikKumaravel_NYCSchools

class RestClientTest: XCTestCase {
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testServiceResponse() {
        let httpClient = RestClient()
        var request = URLRequest(url: URL(string: "https://data.cityofnewyork.us/resource/s3k6-pzi2.json")!)
        request.httpMethod = "GET"
        httpClient.makeGetRequest(request: request, completion: { (data: NSData) in
            XCTAssertNotNil(data)
        }) { (error : NSError) in
            XCTAssertFalse(true, "should not throw error")
        }
    }
    
    func testFailureServiceResponse() {
        let httpClient = RestClient()
        var request = URLRequest(url: URL(string: "http://random")!)
        request.httpMethod = "GET"
        httpClient.makeGetRequest(request: request, completion: { (data: NSData) in
            XCTAssertNotNil(data)
        }) { (error : NSError) in
            XCTAssertFalse(true, "should not throw error even in case of failures")
        }
    }
}

