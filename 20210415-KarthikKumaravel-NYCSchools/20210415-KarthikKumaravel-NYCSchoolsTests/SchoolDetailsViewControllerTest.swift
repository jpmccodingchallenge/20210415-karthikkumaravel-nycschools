//
//  SchoolDetailsViewControllerTest.swift
//  20210415-KarthikKumaravel-NYCSchoolsTests
//
//  Created by Karthik Kumaravel on 4/16/21.
//

import XCTest
@testable import _0210415_KarthikKumaravel_NYCSchools

class SchoolDetailsViewControllerTest: XCTestCase {
    var controller: SchoolDetailsViewController?
    
    override func setUp() {
        super.setUp()
        controller =  UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SchoolDetailsViewController") as? SchoolDetailsViewController
        _ = controller?.view
        XCTAssertNotNil(controller)
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testVieWDidLoad() {
        let param = School(dictionary: ["city": "test", "school_name": "school"])
        controller?.school = param

        controller?.viewDidLoad()
        XCTAssertEqual(controller?.title, "School Details")
        XCTAssertEqual(controller?.titleLabel.text, "school")
        XCTAssertEqual(controller?.descriptionLabel.text, "test")
        XCTAssertNotNil(controller?.school?.dict)
        XCTAssertNotNil(controller?.textView.text)
    }
    
    func testLoadMoreClicked() {
        var param = School(dictionary: ["city": "test", "school_name": "school"])
        param.code = "31R080"
        controller?.school = param

        controller?.loadMoreClicked(UIButton())
        let exp = expectation(description: "Hold for 5 seconds")
         let result = XCTWaiter.wait(for: [exp], timeout: 5.0)
         if result == XCTWaiter.Result.timedOut {
            XCTAssertTrue(controller!.loadMorebutton.isHidden)
         } else {
             XCTFail("Delay interrupted")
         }
    }
    
}
