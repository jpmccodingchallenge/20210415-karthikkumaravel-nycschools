//
//  ServiceManagerTest.swift
//  20210415-KarthikKumaravel-NYCSchoolsTests
//
//  Created by Karthik Kumaravel on 4/16/21.
//

import XCTest
@testable import _0210415_KarthikKumaravel_NYCSchools

class ServiceManagerTest: XCTestCase {
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testSingleton() {
        let manager = ServiceManager.sharedInstance
        let manager2 = ServiceManager.sharedInstance
        
        XCTAssertEqual(manager, manager2)
    }
    
    func testGetSchoolDetails_success() {
        ServiceManager.sharedInstance.getSchoolDetails("31R080") { (response) in
            XCTAssertNotNil(response)
        } failure: { (error) in
            XCTAssertFalse(true, "should not throw error at this stage")
        }
    }
    
    func testGetSchoolDetails_failure() {
        ServiceManager.sharedInstance.getSchoolDetails("random") { (response) in
            XCTAssertFalse(true, "should not throw success at this stage")
        } failure: { (error) in
            XCTAssertNotNil(error)
        }
    }
    
    func testGetSchoolList() {
        ServiceManager.sharedInstance.getSchoolList() { (response) in
            XCTAssertNotNil(response)
        } failure: { (error) in
            XCTAssertFalse(true, "should not throw error at this stage")
        }
    }
    
}
