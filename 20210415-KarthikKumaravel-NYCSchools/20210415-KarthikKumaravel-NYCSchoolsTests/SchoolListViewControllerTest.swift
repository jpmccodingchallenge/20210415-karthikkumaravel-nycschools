//
//  SchoolListViewControllerTest.swift
//  20210415-KarthikKumaravel-NYCSchoolsTests
//
//  Created by Karthik Kumaravel on 4/16/21.
//

import XCTest
@testable import _0210415_KarthikKumaravel_NYCSchools

class SchoolListViewControllerTest: XCTestCase {
    var controller: SchoolListViewController?
    
    override func setUp() {
        super.setUp()
        controller =  UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SchoolListViewController") as? SchoolListViewController
        _ = controller?.view
        XCTAssertNotNil(controller)
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testViewDidLoad() {
        controller?.viewDidLoad()
        XCTAssertEqual(controller?.title, "NYC High Schools")
    }
    
    func testDataSource_return_searchArray() {
        let searchArray = [School(dictionary: ["city": "test"])]
        let schoolList = [School(dictionary: ["city": "school"])]
        controller?.searchArray = searchArray
        controller?.schoolList = schoolList
        controller?.searchBar.text = "test"
        
        XCTAssertEqual(controller?.dataSource?.first?.city, "test")
    }
    
    func testDataSource_return_schoolList() {
        let searchArray = [School(dictionary: ["city": "test"])]
        let schoolList = [School(dictionary: ["city": "school"])]
        controller?.searchArray = searchArray
        controller?.schoolList = schoolList
        controller?.searchBar.text = ""
        
        XCTAssertEqual(controller?.dataSource?.first?.city, "school")
    }
    
    func testTapAction() {
        controller?.tapAction(UIButton())
        let searchBar = controller?.searchBar
        XCTAssertNotNil(searchBar)
        XCTAssertFalse(searchBar!.isFirstResponder)
    }
    
    func testNumberOfRows() {
        let searchArray = [School(dictionary: ["city": "test"])]
        let schoolList = [School(dictionary: ["city": "school"])]
        controller?.searchArray = searchArray
        controller?.schoolList = schoolList
        controller?.searchBar.text = "test"
        
        let rows = controller?.tableView(controller!.tableView, numberOfRowsInSection: 0)
        XCTAssertEqual(rows, 1)
    }
    
    func testCellForRow()  {
        let searchArray = [School(dictionary: ["city": "test"])]
        let schoolList = [School(dictionary: ["city": "school"])]
        controller?.searchArray = searchArray
        controller?.schoolList = schoolList
        controller?.searchBar.text = "test"
        
        let cell = controller?.tableView(controller!.tableView, cellForRowAt: NSIndexPath(row: 0, section: 0) as IndexPath)
        
        XCTAssertNotNil(cell)
    }
    
    func testDidSelectRow() {
        let searchArray = [School(dictionary: ["city": "test"])]
        let schoolList = [School(dictionary: ["city": "school"])]
        controller?.searchArray = searchArray
        controller?.schoolList = schoolList
        controller?.searchBar.text = "test"
        
        controller?.tableView(controller!.tableView, didSelectRowAt: NSIndexPath(row: 0, section: 0) as IndexPath)
        
        XCTAssertNotNil(controller?.selectedSchool)
        XCTAssertEqual(controller?.selectedSchool?.city, "test")
    }
    
    func testSearchBarTextDidBeginEditing() {
        controller?.searchBarTextDidBeginEditing(controller!.searchBar)
        XCTAssertTrue(controller!.tapGesture.isEnabled)
    }
    
    func testSearchBarTextDidEndEditing() {
        controller?.searchBarTextDidEndEditing(controller!.searchBar)
        XCTAssertFalse(controller!.tapGesture.isEnabled)
    }
    
    func testSearchBarTextDidEndEditing_reset() {
        let searchArray = [School(dictionary: ["city": "test"])]
        let schoolList = [School(dictionary: ["city": "school"])]
        controller?.searchArray = searchArray
        controller?.schoolList = schoolList
        controller?.searchBar.text = ""
        
        controller?.searchBarTextDidEndEditing(controller!.searchBar)
        XCTAssertFalse(controller!.tapGesture.isEnabled)
        XCTAssertEqual(controller?.searchArray?.first?.city, "school")

    }
    
    func testSearchBarSearchButtonClicked() {
        controller?.searchBarSearchButtonClicked(controller!.searchBar)
        XCTAssertFalse(controller!.searchBar.isFirstResponder)
    }
    
    func testSearchBarCancelButtonClicked() {
        let searchArray = [School(dictionary: ["city": "test"])]
        let schoolList = [School(dictionary: ["city": "school"])]
        controller?.searchArray = searchArray
        controller?.schoolList = schoolList
        controller?.searchBar.text = "test"
        
        controller?.searchBarCancelButtonClicked(controller!.searchBar)
        XCTAssertEqual(controller?.searchBar.text, "")
        XCTAssertEqual(controller?.searchArray?.first?.city, "school")        
    }
}
