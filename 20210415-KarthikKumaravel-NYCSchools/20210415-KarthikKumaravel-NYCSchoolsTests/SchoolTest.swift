//
//  SchoolTest.swift
//  20210415-KarthikKumaravel-NYCSchoolsTests
//
//  Created by Karthik Kumaravel on 4/16/21.
//
import XCTest
@testable import _0210415_KarthikKumaravel_NYCSchools

class SchoolTest: XCTestCase {
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testAccessors() {
        let param = ["school_name":"Test"]
        let result = School(dictionary: param)
        XCTAssertEqual(result.schoolName, "Test")
        XCTAssertNil(result.code)
        XCTAssertNil(result.overview)
        XCTAssertNil(result.address)
        XCTAssertNil(result.zip)
        XCTAssertNil(result.state)
        XCTAssertNil(result.requirement1_1)
        XCTAssertNil(result.requirement2_1)
        XCTAssertNil(result.email)
        XCTAssertNil(result.phone_number)
        XCTAssertNil(result.website)
        XCTAssertEqual(result.dict, param)        
    }
}
