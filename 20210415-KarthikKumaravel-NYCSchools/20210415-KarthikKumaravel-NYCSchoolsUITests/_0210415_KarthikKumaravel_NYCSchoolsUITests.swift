//
//  _0210415_KarthikKumaravel_NYCSchoolsUITests.swift
//  20210415-KarthikKumaravel-NYCSchoolsUITests
//
//  Created by Karthik Kumaravel on 4/15/21.
//

import XCTest

class _0210415_KarthikKumaravel_NYCSchoolsUITests: XCTestCase {

    override func setUpWithError() throws {
        continueAfterFailure = false
    }

    override func tearDownWithError() throws {
    }

    func testSchoolListScreen() throws {
        let app = XCUIApplication()
        app.launch()
        let label = app.staticTexts["Brooklyn Generation School"]
        let exists = NSPredicate(format: "exists == 1")

        expectation(for: exists, evaluatedWith: label, handler: nil)
        waitForExpectations(timeout: 5, handler: nil)

    }
    
    func testSchoolDEtailsScreen() throws {
        let app = XCUIApplication()
        app.launch()
        let label = app.staticTexts["Brooklyn Generation School"]
        let exists = NSPredicate(format: "exists == 1")

        expectation(for: exists, evaluatedWith: label, handler: nil)
        waitForExpectations(timeout: 5, handler: nil)

        app.staticTexts["Brooklyn Generation School"].tap()
        app.staticTexts["Brooklyn"].tap()
        app.buttons["Load more..."].tap()
        app.children(matching: .window).element(boundBy: 0).children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element(boundBy: 1).tap()
        app.textViews.textViews["40.63445"].tap()
        XCTAssertFalse(app.buttons["Load more..."].exists)
    }
}


